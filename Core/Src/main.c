/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define CAM_I2C_ADDRESS (0x78 << 1)
#define CAM_I2C_ADDRESS_WR 0x78
#define CAM_I2C_ADDRESS_RD 0x79

#define TIMEOUT_I2C 20

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
uint8_t CAM_read_reg   (uint16_t reg_address);
void    CAM_write_reg  (uint16_t reg_address, uint8_t data);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART2 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

uint8_t CAM_read_reg (uint16_t reg_address) {
    uint8_t reg_content[1];
    HAL_I2C_Mem_Read(&hi2c1, CAM_I2C_ADDRESS_RD, reg_address, 2, reg_content,
                        1, TIMEOUT_I2C);
    return *reg_content;
}

void CAM_write_reg (uint16_t reg_address, uint8_t data) {
    HAL_I2C_Mem_Write(&hi2c1, CAM_I2C_ADDRESS_WR, reg_address, 2, &data,
                        1, TIMEOUT_I2C);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  uint8_t reg_content[1] = {0};

  printf("Tests d'initialisation PCAM-5C\r\n");
  printf("------------------------------\r\n");

  // 1. Execute a power-cycle by applying a low pulse of 100ms on CAM_PWUP, then driving it high.
  HAL_GPIO_WritePin(CAM_UP_GPIO_Port, CAM_UP_Pin, GPIO_PIN_RESET);
  HAL_Delay(100);
  HAL_GPIO_WritePin(CAM_UP_GPIO_Port, CAM_UP_Pin, GPIO_PIN_SET);

  // 2. Wait for 50ms.
  HAL_Delay(50);

  if (HAL_I2C_IsDeviceReady(&hi2c1, CAM_I2C_ADDRESS_RD, 3, TIMEOUT_I2C) == HAL_OK) {
      printf("I2C READY 0x%02X\r\n", CAM_I2C_ADDRESS_RD);
  }

  else {
      printf("I2C ERROR 0x%02X\r\n", CAM_I2C_ADDRESS_RD);
  }

  // 3. Read sensor ID from registers 0x300A and 0x300B and check
  // against 0x56 and 0x40, respectively.
  *reg_content = CAM_read_reg(0x300A);
  printf("0x300A: 0x%02X", *reg_content);
  if (*reg_content == 0x56) {
      printf(" OK\r\n");
  }
  else {
      printf(" ERROR\r\n");
  }

  *reg_content = CAM_read_reg(0x300B);
  printf("0x300B: 0x%02X", *reg_content);
  if (*reg_content == 0x40) {
      printf(" OK\r\n");
  }
  else {
      printf(" ERROR\r\n");
  }

  // 4. Choose system input clock from pad by writing 0x11 to register
  // address 0x3103.
  CAM_write_reg(0x3103, 0x11);
  printf("0x%04X: 0x%02X\r\n", 0x3103, CAM_read_reg(0x3103));

  // 5. Execute software reset by writing 0x82 to register address 0x3008.
  CAM_write_reg(0x3008, 0x82);
  printf("0x%04X: 0x%02X\r\n", 0x3008, CAM_read_reg(0x3008));

  // 6. Wait for 10ms.
  HAL_Delay(10);

  // 7. De-assert reset and enable power down until configuration is done
  // by writing 0x42 to register address 0x3008.
  CAM_write_reg(0x3008, 0x42);
  printf("0x%04X: 0x%02X\r\n", 0x3008, CAM_read_reg(0x3008));

  // 8. Choose system input clock from PLL by writing 0x03 to register address 0x3103
  CAM_write_reg(0x3103, 0x03);
  printf("0x%04X: 0x%02X\r\n", 0x3103, CAM_read_reg(0x3103));

  // 9. Set PLL registers for desired MIPI data rate and sensor timing (frame rate).
  // TODO: Déterminer les registres à écrire

  // 10. Set imaging configuration registers
  // TODO: Déterminer les registres à écrire

  // 11. Enable MIPI interface by writing either 0x45 for two-lane mode or 0x25 for
  // one-lane mode to register address 0x300E.
  CAM_write_reg(0x300E, 0x45);
  printf("0x%04X: 0x%02X\r\n", 0x300E, CAM_read_reg(0x300E));

  // 12. Let MIPI clock free-run, and force LP11 when no packet transmission by writing
  // 0x14 to register address 0x4800.
  CAM_write_reg(0x4800, 0x14);
  printf("0x%04X: 0x%02X\r\n", 0x4800, CAM_read_reg(0x4800));

  // 13. Set output format to RAW10 by writing 0x00 to register address 0x4300 and 0x03
  // to register address 0x501F.
  CAM_write_reg(0x4300, 0x00);
  CAM_write_reg(0x501F, 0x03);
  printf("0x%04X: 0x%02X\r\n", 0x4300, CAM_read_reg(0x4300));
  printf("0x%04X: 0x%02X\r\n", 0x501F, CAM_read_reg(0x501F));

  // 14. Wake up sensor by writing 0x02 to register address 0x3800.
  CAM_write_reg(0x3800, 0x02);
  printf("0x%04X: 0x%02X\r\n", 0x3800, CAM_read_reg(0x3800));

  printf("Power-up procedure complete.");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
        HAL_Delay(1000);
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    printf("Error handler\r\n");
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
